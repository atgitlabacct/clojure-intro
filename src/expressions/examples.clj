(ns expressions.examples)

;; Everything in Clojure is an expression
;;  expressions always retunr a value
;;  let, do, fn returns the last value
;;  Expressions exclusively for side-effects return nil


(if true :truthy :falsey)
(if (Object.) :truthy :falsey)
(if [] :truthy :falsey)

(if false :truthy :falsey)
(if nil :truthy :falsey)
(if (seq []) :truthy :falsey)

(str "2 is " (if (even? 2) "even" "odd"))
(str "3 is " (if (even? 2) "even" "odd"))

(if false :truthy) ;; nil


;; Allow us to 'do' multiple things in an if expression
(if (even? 5)
  (do (println "even")
      true)
  (do (println "odd")
      false))

(defn show-evens
  "show show"
  [coll]
  (if-let [evens (seq (filter even? coll))]
    (println (str "The evens are: " evens))
    (println "There are no evens")))

(show-evens [2 5 7])
(show-evens [1 5 7])
(show-evens [2 4 6 5 7])
