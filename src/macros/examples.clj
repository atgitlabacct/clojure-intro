(ns macros.examples)

(import [java.lang Thread])

(defmacro with-new-thread
  [& body]
  "Start a thread and run body" 
  `(.start (Thread. (fn [] ~@body))))
