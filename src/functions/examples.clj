;; I need to user user namespace because of class path bullshit
;; My guess is because I don't have a proper project.clj setup
(ns functions.examples)

(let [x 10
      y 20]
  (+ x y))

;;(defn messenger "Messeging to console" 
;;  ([] (print "Hello World\n"))
;;  ([m] (print m)))
(require '[clojure.string :as string])
(defn messenger "Messeging to console" 
  ([] (print "Hello World\n"))
  ([m & who] (print m (string/join " " who)))) ;; who will be a sequence

(messenger)
(messenger "John Doe" "world" "class") ;; Variatic function


(defn messenger-builder "Bulds a messenger" [greeting]
  (fn [who] 
    (let [foo "Jebus"]
      (apply print greeting foo who))))

(def hello-er (messenger-builder "Hello"))

(hello-er "world")
