(ns namespaces.examples
  (:require [clojure.set])
  )

(clojure.set/union #{1 2} #{3 4 5})

(require '[clojure.set :as set])
(set/union #{1 2} #{3 4 5})


(use '[clojure.string :only (join)])
(join "-" ["foo" "bar"])


(defn do-union [& sets]
  (apply set/union sets))

(do-union #{ 1 2 3 } #{ 9 10 11 })
