(ns destructuring.examples)
;; Declarative way to pull apart compound data
;; Works for both sequential and associative data structures

;; Sequential Destructuring

(def stuff [7 8 9 10 11])
;; Bind a, b, c to first 3 values
(let [[a b c] stuff]
  (list (+ a b) (+ b c)))

;; f should be nil
(let [[a b c d e f] stuff]
  (list d e f))

;; Get everything else with &
(let [[a & others] stuff]
  (println a)
  (println (clojure.string/join "-" others)))

;; Destruct and ignore the 0th and 2th values
;; The underscore is purely idiom in clojure to show which to ignore
(let [[_ sec _ & others] stuff]
  (println sec)
  (println others) ;; Notice this is a list
  (println (vec others)) ;; Lets make it a vector
  (println (clojure.string/join "--" others) ))

;; Destructure associative collections
(def m {:a 7 :b 4})

;; Key is missing then nil
(let [{val_a :a val_b :b val_c :c} m]
  [val_a val_b val_c])

;; Another way to access keys
(let [{:keys [a b c]} m]
  [a b c])

(let [{:keys [a b c]
       :or {c 101}} m]
  [a b c])
;; Inside let vecotr []  we are simply using a map
;; the maps can take a number of diff options
(let [{val_a :a val_b :b val_c :c :or {val_c 101}} m]
  [val_a val_b val_c])


(defn game
  "Some game"
  [_planet & { :keys [human-players computer-players] }]
  (println "Total players: " (+ human-players computer-players)))

(defn game2
  "Some game"
  [_planet & { human-players :human-players computer-players :computer-players }]
  (println "Total players: " (+ human-players computer-players)))

(game "Mars" :human-players 2 :computer-players 4)
(game2 "Mars" :human-players 2 :computer-players 1)


(defn draw-point
  "doc string"
  [_place & {:keys [x y z]
             :or {x 0 y 0 z 0} }]
  [x y z])

(draw-point "Some where over the rainbox"  :x 10 :y 20)


;; Generate a patient
(def patient
  {:name "John Doe"
   :age 33
   :phone "555-555-555"
   :supplies {
              :mask {:name "Resmed mask"
                     :hcpc "MASK001"}
              :tubing {:name "Tubing"
                       :hcpc "TUBE001"}
              :filter {:name "Filter"
                       :hcpc "FILTER001"}
              }
   })

(let [{{:keys [mask tubing] } :supplies} patient]
  (println mask)
  (println tubing)
  )

(defn get-supply
  "Gets a supply"
  [key data]
  (let [{{value key} :supplies} data]
    value))

(defn get-mask
  "Gets a mask given a patient abstraction"
  [p]
  (get-supply :mask p))

(get-supply :mask patient)
(get-supply :tubing patient)
(get-mask patient)
