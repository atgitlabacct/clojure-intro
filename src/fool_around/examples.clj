(ns fool-around.examples)

(def default-data { :name "John Doe" :age 0 })

(defn do-something
  "Does do-something"
  [default-data d]
  (println default-data)
  (println "Default data" default-data " -- " d))


(do-something [1 2 3] [4 5 6])

(println "-------------------")

(defn get-location
  "Dummy location"
  []
  {:lat 45.6 :long 101.1})

(defn find-city-name-by-location
  "Dummy find"
  [location]
  "Oconto, WI")

;; Using _ as an ignore and just printing original location value, and then we
;; redefine the location value.
(let [location (get-location) 
      _ (println "Location " location)
      location (find-city-name-by-location location)]
  (println "City name is " location))

;; Nested let has a different binding vector?
;; it appears it is not overwritten.
(let [foo "foo"]
  (println "foo is " foo)
  (let [foo "bar"]
    (println "Foo is " foo))
  (println "Foo is " foo))

