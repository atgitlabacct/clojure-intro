(ns concurrency.reads-no-tx
  (:require [macros.examples :as me]))

(def r (ref 0))

;; So we must be careful reading without a transaction because
;; if you are reading a ref multiple times you coud get different values
;; down the call chain.
(comment 
  (me/with-new-thread
    (dotimes [_ 10]
      (Thread/sleep 1000)
      (dosync
        (alter r inc))
      (println "updated ref to " @r)))

  (me/with-new-thread
    (dotimes [_ 7]
      (println "ref is " @r)
      (Thread/sleep 1000))))
