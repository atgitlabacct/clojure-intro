(ns concurrency.validators)

(comment
  (def ^:dynamic thingy 17)
  (set-validator! (var thingy) #(not= %1 16))

  (binding [thingy 20] (println thingy))
  (binding [thingy 16] (println thingy))

  (println thingy)

  ;; Clears it
  (set-validator! (var thingy) nil))
