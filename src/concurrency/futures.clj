(ns concurrency.futures)

(def my-future
  (future
    (Thread/sleep 5000)
    (println "Doint stuff")
    (Thread/sleep 3000)
    17))

(eval @my-future)
