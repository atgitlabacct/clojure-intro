(ns concurrency.reads_with_tx
  (:require [macros.examples :as me]))

(comment
  (def r (ref 0))

  (me/with-new-thread
    (dotimes [_ 10]
      (Thread/sleep 1000)
      (dosync
        (alter r inc))
      (println "updated ref to " @r)))

  ;; We want to repeat a read
  ;; We are still seeing a picture of the world once we hit the
  ;; dosync below when reading the ref
  (me/with-new-thread
    (println "r outside is " @r)
    (dosync
      (dotimes [i 7]
        (println "iter is " i)
        (println "ref is " @r)
        (Thread/sleep 1000)))
    (println "r outside is " @r)))
