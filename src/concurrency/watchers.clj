(ns concurrency.watchers)

(defn my-watcher
  ""
  [key r old new]
  (println "We ran" key r old new))

(def foo (atom 3))

(add-watch foo :my-key my-watcher)

(swap! foo inc)

(remove-watch foo :my-key)

