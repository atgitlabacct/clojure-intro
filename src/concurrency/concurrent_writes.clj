(ns concurrency.concurrent-writes
  (:require [macros.examples :as me]))

(def r (ref 0))

;; Interesting stuff here
;; Since our first thread here has a sleep of 5 seconds, but already
;; started changing the value, the second thread has to wait and retry
;; its update until the first thread is done.
;;
;; Very similiar to how transactions work.  We are seeing the STM at work.
(me/with-new-thread
  (dosync 
    (println "tx1 initial:" @r)
    (alter r inc)
    (println "tx1 final:" @r)
    (Thread/sleep 5000)
    (println "tx1 done")))

(me/with-new-thread
  (dosync
    (println "tx2 initial:" @r)
    (Thread/sleep 1000)
    (alter r inc)
    (println "tx2  final:" @r)
    (println "tx2 done")))
