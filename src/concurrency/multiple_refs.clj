(ns concurrency.multiple_refs
  (:require [macros.examples :as me]))

(def r1 (ref 0))
(def r2 (ref 0))

;; Atomic operation in the dosync
(me/with-new-thread
  (dotimes [_ 10]
    (dosync
      (alter r1 inc)
      (Thread/sleep 500)
      (alter r2 inc))
    (println "updated r1/r2 to " @r1)))

(me/with-new-thread
  (dotimes [_ 10]
    (println "r1=" @r1 "  r2=" @r2)
    (Thread/sleep 500)))
