(ns concurrency.promises)

(def my-promise (promise))

(future
  (println "Wiating for promise to be delivered...")
  (println "Delivered: " @my-promise))

(deliver my-promise 42)

(deref my-promise)

;; Does not change
(deliver my-promise 17)
(deref my-promise)

;; Deadlock possible

(def promise1 (promise))

(def promise2 (promise))

(defn prom2
  []
  (future
    (println "Wiating for promise2: ")
    (println @promise2)
    (deliver promise1)))

(defn prom1
  []
  (future
    (println "Wiating for promise1: ")
    (println @promise1)
    (deliver promise2)))
