(ns concurrency.examples)

;; vars

(comment
  (def ^:dynamic me { :name "Adam" :age 33 })

  (defn print-me
    "prints var"
    ([] (print-me ""))
    ([prefix] (println prefix me)))

  (print-me)
  (print-me "Map = ")


  ;;(binding [me 3]
  ;;  (print-me)
  ;;  )


  (import [java.lang Thread])

  (defn with-new-thread
    "doc string"
    [f]
    (.start (Thread. f)))

  (with-new-thread 
    (fn [] (print-me "new thread")))

  ;; The bad spock universe
  (do
    (binding [me "Ginger"]
      ;; This is not like the others
      ;; Because we are spanning off into a new thread...with its own threadspace
      ;; this is bound to the root var bindings..in this case the map above, no
      ;; the value Ginger.
      (with-new-thread 
        (fn [] (print-me "background:")))
      ;; This will use the var with value Ginger because its inside the binding
      (print-me "foreground1:"))
    ;; This prints original value becuase its outside of the binding.
    (print-me "foreground2:"))


  ;; Mutable References
  ;; Freeze the world
  ;; Look at it via dref or @ reader macro
  ;; Only thing that mutates outside of Java interop (and transients)

  ;; Atoms
  ;; Manages an independent value
  ;; State changes thru swap! using ordinary function
  ;; Change occurs synchronously on called thread
  ;; Models compare-and-set (CAS) spin swap
  ;; Functions may be called more then once if something changed before update
  ;; Gaurenteed atomic transition - must avoid side effects (eg. logging could happen multiple times)
  (def foo (atom {:name "Adam"}))
  (deref foo)
  (println @foo)

  (swap! foo (fn [old] {:name "Ginger"}))
  (println @foo)

  (def bar (atom 10))

  ;; This gets all the data, but not in order
  (pmap
    (fn [_] (swap! bar inc))
    (range 10))

  ;; Lets do a little digging here
  ;; Set a counter to see how often functions will get called
  (def call-counter (atom 0))

  (defn slow-inc-with-call-count
    "Increment slowly"
    [x]
    (swap! call-counter inc)
    (Thread/sleep 100)
    (inc x))

  ;; The interesting thing here is how often call-counter gets called
  ;; Since each thread is working on call-counter it had to recall
  ;; itself multiple times to be atomic
  (pmap
    (fn [_] (swap! bar slow-inc-with-call-count))
    (range 20))

  (deref call-counter)

  ;; Agents
  ;; Manage an independent value
  ;; Changes thru ordinary function executed asynchronously
  ;; NOT actors: NOT distributed
  ;; Use send or send-off to dispatch
  ;; Clojure runs agents on a thread from the thread pool

  (def my-agent
    (agent {:name "adam-trepanier"
            :favorites []}))


  (defn slow-append-favorite
    "doc string"
    [val new-fav]
    ;; Slow the agent down 
    (Thread/sleep 2000)
    (assoc val
           :favorites
           (conj (:favorites val) new-fav)))

(defn test-agent
  []
  (do
    (send my-agent slow-append-favorite "food")
    (send my-agent slow-append-favorite "beer")
    (send my-agent slow-append-favorite "vodka")
    (send my-agent slow-append-favorite "scotch")
    (println @my-agent)
    ;; Wait just a tad for the agent to catch up
    (Thread/sleep 2500)
    (println @my-agent)
    (Thread/sleep 2500)))
(println @my-agent)

(str @my-agent)


(def erroring-agent (agent 3))

(defn modify-agent-with-error
  "doc string"
  [current new]
  (if (= 42 new)
    (throw (Exception. "Not 42!!"))
    new))

(deref erroring-agent)

(send erroring-agent modify-agent-with-error 42)

(send erroring-agent modify-agent-with-error 12)

;; Show errors on agent
(agent-errors erroring-agent)

;; Clears the agent errors
(clear-agent-errors erroring-agent)

;; Refs
;; Allow for synchronous changes to shared state
;; Can only be changed within a transaction
;; STM (Software Transactional Memory) system
;; Retries are automatic
;; Should be no side effects
;; Can compose with agents, allowing deferred side effects

(def foo (ref
           {:first "Adam"
            :last "Trepanier"
            :children 2}))

;; Returns a new map, doesn't update the ref, only reads
(assoc @foo :blog "http://www.adamtrepnaier.com")

(deref foo)

;; dosync makes the transaction (STM)
;; without dosync you would get an error
(dosync
  (commute foo assoc
           :blog
           "http://www.adamtrepanier.com"))

(dosync
  (alter foo dissoc :bloc) ;; Removes something, no error if it doesn't exist
  (alter foo assoc
         :blog
         "http://www.needtoupdatemybloc.com")))
