(ns sequences.examples
  )

(seq [1 2 3]) ;; => (1 2 3)   THIS IS NOT A LIST
(rest(seq [1 2 3]))
(first [1 2 3])

;; cons - stand for construct, puts items and beginning of sequence
(cons 1 (seq [1 2 3]))
(cons 1 (rest (seq [1 2 3])))

;; Notice this comes out a sequence
;; Some of the time functions call (seq x) on your be half
(cons 0 [1 2 3])

(rest {:a "a" :b "b" :c "c"})

;; Lazy sequence
(range 1 4)
;; Points to a lazy sequence
;; None of the numbers have been generated
;; This is a linked list with the next element pointing to a function of what
;; to generate next
;;   ---  ---                 ---  ---
;;  | 1 | fn | ---- fn ----> | 2 | fn |
;;   ---  ---                 ---  ---
;; fn is not ran until its needed, so sequences can be infinite
(def a-range (range 1 4))

; REPL prints a seq with open and closing parens, but these are not
;; lists.  Infinite sequences take a long time to print.
(set! *print-length* 10) ; only prints 10 things
;; if you run   (range 1 1000)   the repl prints (1 2 3 4 5 6 7 8 9 10 ...)

(take 3 (range 1 100))
(take 10 (range 1 100))
(drop 10 (range 1 100))
(filter even? ( range ))
(apply str (interpose "," (range 8)))
(map #(* % %) (range 10))

(def fibs 
  (map first 
       (iterate (fn [[a b]] 
                  [b (+ a b)])
                [0 1])))

(take 5 fibs)
(nth fibs 9)
