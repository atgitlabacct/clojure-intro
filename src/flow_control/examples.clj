(ns flow-control.examples)

;; cond - condition
;; series of tests and expressions
;; :else expression is optional
(let [x 15]
  (cond
    (< x 2) "x is less then 2"
    (< x 10) "x is less then 10"
    (< x 20) "x is less then 20"
    (< x 30) "x is less then 30"
    :else "s is greater then or eql to 10"))

;; condp - condition with predication
;; similiar to cond execpt predicate is one time in condp
(defn test-condp 
  "doc string"
  [x]
  (condp = x
    5 "x is 5"
    10 "x is 10"
    "x isn't 5 or 10"))

(test-condp 7)
(test-condp 5)


;; case
;; predicate always =
;; test values must be compile time literals
;; Matchis O(1)
(defn test-case
  "doc string"
  [x]
  (case x
    5 "x is 5"
    10 "x is 10"
    "x isn't 5 or 10"))

(test-case 11)
(test-case 5)
;; Iterates over a sequence
;; if a sequence it forces evaluation
(doseq [n (range 3)]
  (println n))

(dotimes [i 3]
  (println i))


;; for
;; list comprehension, NOT a for loop
;; Generator function for seq permutation
(for [x [0 1]
      y [0 1]
      z [8 9]]
      [x y z])


(println "-------------")
;; recur
;; loop is considered low level in clojure
;; fn arguments are binding and we can use recur to recall a function
;; recur must be in tail position - last expression in the branch
;; Recurions via recur does NOT consume stack
(defn increase
  "doc string"
  [i]
  (if (< i 10)
    (recur (inc i)) ;; Notice recur provides values for defn/fn args
    i))

(increase 5)


(defn factorial
  ([n] (factorial 1 n))
  ([accum n]
   (if (zero? n)
     accum
     (recur (*' accum n) (dec n)))))

(factorial 100)
