(ns collections.examples)

;; Lists
;; Sequential
(list 1 2 3) ;; (1 2 3)
'(1 2 3) ;; need to quotes because compiler trys to evalute the list
(conj (list 1 2 3) 4)
(conj '(1 2 3) 4)


;; Vectors
;; Sequential & Associative
;; Associative because we can do by location
[1 2 3] ;; Notice no need to quote, vector is not a list, so no evaluation

;; lets use vector instead of []
;; Also notice conj adds 0 to the end instead of head.
;; conj will add the item efficently as possible
(conj (vector 1 3 4) 0)
(vec '(1 2 3)) ;; Make the list a vector


;; Maps
;; Maps need distint keys
;; Associative because we can do by key
{}
{ :a 1 :b 2 }  ;; Again no quoting because we are not a list...evalutor doesn't evaluate
(:a {:a 10 :b 100}) ;; Using key in the function position
({:a 10 :b 100} :b)
(assoc {:a 1} :b 2) ;; Add :b
(dissoc {:a 1 :b 2} :b 2) ;; Remove :b
(conj {:b 100} [:a 1]) ;; Add :a -- Notice a is added to the front

;; Notice def and not defn
;; def n gives a name to a value and is only evaluated once
;; defn is evaluted multiple times
(def jdoe { :name "John Doe" :address { :zip 27705 }  })

;; Accessing  maps
(get-in { :name "John Doe" :phone "920-834-3336"  } [:name])
(get-in jdoe [:name])
(get-in jdoe [:address])

;; Updating maps
(assoc-in jdoe [:address :zip] 54153)
(update-in jdoe [:address :zip] inc) ;; inc is an incrementer
(assoc-in jdoe [:address :suite] "")
(assoc (jdoe :address) :suite "" :foo "bar")
(assoc jdoe :age 34 :phone "555-555-555")

;; Sets
;; We can use these to check membership
;; - contains distint values
;; - Insertion O(1) - Constant Time
;; - Member? O(1)
;; - they are also unordered
#{ :a :b :c }
(#{ :a :b :c} :b )
(#{ :a :b :c } :b )
(conj #{ } :a)
(contains? #{ :a } :a)
(contains? #{ :b } :a)
