(ns collections.map-destruc)

;; Some practice on breaking apart a map

(def data
  {:name "Adam Trepanier"
   :date-of-birth "1981-04-30"
   :gender "male"
   :patient-key "test-adam"
   :status "active"
   :phone "555-555-5555"
   :shipping-address {:street "123 Foo Bar Way"
                      :city "Somwhere"
                      :state "CA"
                      :postal-code "55555"
                      }
   :orders [
            {:order-key "GHF12D3"
             :branch-code "01-Machinaw"
             :ordered-on "2014-01-20"
             :supply-part-key "CPAP 2"
             :status "closed"
             :supply-category-key "unit"
             :order-type "D"}
            {:order-key "FDJ900"
             :branch-code "02-Oconto"
             :ordered-on "2014-08-05"
             :supply-part-key "tube2"
             :status "closed"
             :supply-category-key "tubing"
             :order-type "P"}
            {:order-key "ADE678"
             :branch-code "01-Machinaw"
             :ordered-on "2014-05-16"
             :supply-part-key "mask2"
             :status "open"
             :supply-category-key "mask"
             :order-type "D"}
            {:order-key "ZXC098"
             :branch-code "03-Brownsvile"
             :ordered-on "2014-01-02"
             :supply-part-key "CPAP 2"
             :status "closed"
             :supply-category-key "unit"
             :order-type "P"}]})

;; Lets do some destructoring

(keys data) ;; Outputs the first keys

(keys {data :orders}) ;; Outputs the keys inside of orders

;; The parens on (data :orders) works because Clojure treats a map as a function
;; we could also do (:orders data)
;; (:key map) vs (map :keys) returns nil vs error
(:orders data)

(get 
  (first (data :orders)) ;; Lets get the first order...notice the parens
  :order-key)

(keys (first (data :orders))) ;; Output just the order keys of the first map in orders

(defn orders-with-type
  "Return a list of orders given a specific order-type"
  [o-type orders]
  (filter
    #(= (% :order-type) o-type)
    orders))

(orders-with-type "P" (data :orders))

(defn delivery-orders
  "Return the list of delivery orders"
  [orders]
  (orders-with-type "D" orders))

(defn pickup-orders
  "Return a list of pickup orders"
  [orders]
  (orders-with-type "P" orders))

(pickup-orders (data :orders))
(delivery-orders (data :orders))

;; pass the vector of keys to take
;; not a map 
(let [{:keys [name date-of-birth]} data]
  (println name " -- " date-of-birth))

;; Lets use our delivery orders function
(let [delivery-orders (delivery-orders (data :orders))]
  (println delivery-orders))

;; Lets pull only the order-keys
(defn unique-order-trait
  "Given a seq of orders we will return a set of order-keys"
  [orders, key]
  (reduce
    (fn [_set o]
      (conj _set (o key)))
    #{}
    orders))

;; Lets grab all the order keys
(let [order-keys (unique-order-trait (data :orders) :order-key)]
  (println order-keys))

(let [traits (unique-order-trait (data :orders) :supply-category-key)]
  (println traits))

(let [traits (unique-order-trait (data :orders) :status)]
  (println traits))

;; Destructor a map with another map -> patient has a shipping address
;; We use the :keys directive to to assign the shipping address info inside of data
;; Notice we also use the :keys directive to also get the name and date of birth
(let [{:keys [name date-of-birth orders]
       {:keys [street city state postal-code]} :shipping-address
       :as patient} data]
  (println name \- date-of-birth)
  (println street ", " city ", " state "  " postal-code)
  (println orders)
  (println "------- Full Patient Deatils -----"))

;; In Clojure 1.6 we can now prefix the name of a key if its the same
;; A litte more context with the prefix, but still very clean
(let [{:keys [name date-of-birth orders
              shipping-address/street  shipping-address/city
              shipping-address/state shipping-address/postal-code]
       :as patient} data]
  (println name \- date-of-birth)
  (println street ", " city ", " state "  " postal-code)
  (println orders)
  (println "------- Full Patient Deatils -----")
  (println patient))

;; In Clojure 1.6 we can now also auto resolve keyword forms in the :keys directive
;; I think you lose some context with auto-resolve, but its the shortest code
(let [{:keys [name date-of-birth orders
              ::street  ::city ::state ::postal-code]
       :as patient} data]
  (println name \- date-of-birth)
  (println street ", " city ", " state "  " postal-code)
  (println orders)
  (println "------- Full Patient Deatils -----")
  (println patient))
