(defproject intro "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [expectations "2.0.9"]]
  :repl-options { :port 4001 }
  :min-lein-version "2.0.0"

  :profiles {:dev {:dependencies []
                   :plugins [[lein-expectations "0.0.7"]
                             [lein-autoexpect "1.4.0"]]}})
